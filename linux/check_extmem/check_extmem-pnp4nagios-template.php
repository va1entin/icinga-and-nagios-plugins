<?php

#
# An extended memory check for Linux which watches physical and swap memory.
# This is a sample pnp4nagios template.
#

#
# MIT License
#
# Copyright (c) 2015 Steffen Schoch <mein@gehirn-mag.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

# 2019-02-15, schoch: Init...


$l = 24;          // label length
$f = '%3.2lf GB'; // label format

# generel settings
$opt[1] = " --vertical-label \"GigaByte\" --title \"Extended Memory for $hostname\" --lower-limit 0 ";
$ds_name[1] = 'ext. mem';

# var definitions
$def[1]  = rrd::def('pUsed', $RRDFILE[1], $DS[1], 'AVERAGE');
$def[1] .= rrd::def('pFree', $RRDFILE[2], $DS[2], 'AVERAGE');
$def[1] .= rrd::def('pBuffer', $RRDFILE[3], $DS[3], 'AVERAGE');
$def[1] .= rrd::def('pCache', $RRDFILE[4], $DS[4], 'AVERAGE');
$def[1] .= rrd::def('sUsed', $RRDFILE[5], $DS[5], 'AVERAGE');
$def[1] .= rrd::def('sFree', $RRDFILE[6], $DS[6], 'AVERAGE');

# areas and legends
$def[1] .= rrd::area('pUsed', '#ff0000', rrd::cut('physical used', $l), ':STACK');
$def[1] .= rrd::gprint('pUsed', array('LAST', 'AVERAGE', 'MAX'), $f);
$def[1] .= rrd::area('pFree', '#00ff00', rrd::cut('physical free', $l), ':STACK');
$def[1] .= rrd::gprint('pFree', array('LAST', 'AVERAGE', 'MAX'), $f);
$def[1] .= rrd::area('pBuffer', '#ffff00', rrd::cut('physical buffer', $l), ':STACK');
$def[1] .= rrd::gprint('pBuffer', array('LAST', 'AVERAGE', 'MAX'), $f);
$def[1] .= rrd::area('pCache', '#0000ff', rrd::cut('physical cache', $l), ':STACK');
$def[1] .= rrd::gprint('pCache', array('LAST', 'AVERAGE', 'MAX'), $f);
$def[1] .= rrd::area('sUsed', '#ff8888', rrd::cut('swap used', $l), ':STACK');
$def[1] .= rrd::gprint('sUsed', array('LAST', 'AVERAGE', 'MAX'), $f);
$def[1] .= rrd::area('sFree', '#88ff88', rrd::cut('swap free', $l), ':STACK');
$def[1] .= rrd::gprint('sFree', array('LAST', 'AVERAGE', 'MAX'), $f);

// last line
if($this->MACRO['TIMET'] != ""){
    $def[1] .= "VRULE:".$this->MACRO['TIMET']."#000000:\"Last Service Check \\n\" ";
}

