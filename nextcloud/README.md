## check_nextcloud.py

A simple update checker for nextcloud. Creates CRITICAL alerts if a new update
is available. This checker uses the same update mechanism as nextcloud itself.
There is also a sample config for icinga2.
```
usage: check_nextcloud.py [-h] -f FILE -c CONFIG [-p PHP] [--updateServer UPDATESERVER]
                          [-v]

A simple Nextcloud-Update Check

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  full path to version.php
  -c CONFIG, --config CONFIG
                        full path to config.php (in most cases something like
                        .../config/config.php)
  -p PHP, --php PHP     full path to your php binary (default /usr/bin/php)
  --updateServer UPDATESERVER
                        Base URL for Update Server (default
                        https://updates.nextcloud.org/updater_server/)
  -v, --verbose         be more verbose

```

![alt text](https://gehirn-mag.net/wp-content/uploads/2019/01/Bildschirmfoto_2019-01-03_07-48-12-1.png "Output in icingaweb2")

You could visit my blog for more details at [https://gehirn-mag.net/nextcloud-update-check/](https://gehirn-mag.net/nextcloud-update-check/) (German only).

